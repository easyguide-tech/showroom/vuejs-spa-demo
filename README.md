Vuejs SPA Demo
===
EasyGuide Vuejs SPA demo (Bootstrap)
- Powered by EasyGuide.tech Inc
- version 0.1
> **Alert note:**  This application is for demonstration purpose only and it is not intended for use in a specific production. It is too generic and not designed to be particularly efficient, stable, or secure. It does not support all the required features and specific needs of a complete production-ready solution!

---
Website : https://easyguide.tech

![EasyTech_IDE_handbook.jpg](EasyGuide_handbook.jpg)

---

<!-- TOC -->

- [1. Features](#1-features)
- [2. Setup](#2-setup)
- [3. Usage](#3-usage)
    - [Compiles and hot-reloads for development](#compiles-and-hot-reloads-for-development)
    - [Compiles and minifies for production](#compiles-and-minifies-for-production)
    - [Lints and fixes files](#lints-and-fixes-files)
    - [Customize configuration](#customize-configuration)
- [4. Copyright and License](#4-copyright-and-license)

<!-- /TOC -->

---
# 1. Features
---
![preview-demo.gif](preview-demo.gif)

* Lorem ipsum
* Lorem ipsum
* Lorem ipsum


---
# 2. Setup
---

```
npm install
```

---
# 3. Usage
---

## Compiles and hot-reloads for development
```
npm run serve
```

## Compiles and minifies for production
```
npm run build
```

## Lints and fixes files
```
npm run lint
```

## Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

---
# 4. Copyright and License
---
* This project is released under the MIT expat license and the EasyGuide.tech EE LICENSE
- see the [LICENSE.md](LICENSE.md) file for details.
- see the [EE-LICENSE.md](ee/EE-LICENSE.md) file for details.

The package is Open core Source released under the [MIT Expat](LICENSE) with DCO requirement for contributing (Developer Certificate of Origin).

It is developed by cheikhna diouf and derived from his official book "Easy Guide ! A new method" which you can buy as printed book or as ebook on official website, Amazon, Google Play and the iBooks Store.

![EasyGuide_Collection.jpg](EasyGuide_Collection.jpg)


